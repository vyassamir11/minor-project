import re,enchant,html2text,os,time
from textblob import TextBlob

debug = 0
sentence_threshold = 5
skip_time = 300
not_found = []
log = ""
dictionary = enchant.Dict("en_US")
#FIXME: add non trivial words to Dictionary
non_trivial = open("nontrivial.txt","r").read().split("\n")
for _line in non_trivial:
    dictionary.add(_line.strip().lower())
custom_stop = set(open("custom_stop.txt","r").read().lower().split("\n"))


def get_nouns(text):
    if isinstance(text,str):
        blob = TextBlob(text)
    else:
        blob = TextBlob(" ".join(text))
    nouns = []
    for (word,tag) in blob.tags:
        if tag[0] == "N":
            nouns.append(word)
    return nouns


def clean_word(word):
    #reomves punctuations from a word, ignores weirdly Unicode chars like Ã,È, etc
    try:
        word = word.encode().decode('unicode_escape')
    except:
        #print("Codec exception for : "+str(word))
        pass
    #end of sentence word, useful for further analysis
    if word[-1] == ".":
        return word[:-1]
    return re.sub("\W+","",word)


def filter_data(data):
    global not_found,counter_not_found,counter_threshold,debug,skip_time
    broke = 0
    start_time,end_time = time.time(),time.time()
    #gives legit words from data, preserves the lines
    out,start = "",0
    i = 0
    while(data.find("\n",start) != -1):
        end_time = time.time()
        if end_time-start_time > skip_time:
            broke = 1
            break
        i += 1
        if debug:
            print(i,"start")
        end = data.index("\n",start)
        if debug:
            print(i,"step1")
        line = data[start:end].split()
        if debug:
            print(i,"step2")
        #clear all the words of line
        line = list(map(clean_word,line))
        if debug:
            print(i,"step3")
        #ensure line does not have any empty strings
        line = list(filter(lambda x : x != "",line))
        if debug:
            print(i,"step4 len : ",len(line))
        #new_line = list(filter(lambda x : dictionary.check(x),line))
        new_line = []
        for _word in line:
            if debug:
                print(i,"step4a start",_word)
            # word should not contain digits
            if (re.findall(r'[0-9]',_word) == []) and (_word not in custom_stop) and dictionary.check(_word) :
                new_line.append(_word)
                if debug:
                    print(i,"step4b hit",_word)
            else:
                if _word in custom_stop:
                    not_found.append(_word)
                    if debug:
                        print(i,"step4b miss",_word)
        if debug:
            print(i,"step5")
        out += " ".join(new_line)+"\n"
        start = end+1

        if debug:
            print(i,"end")
    #uniform line gap
    out = re.sub("\n+",r"\n",out)
    return (out,broke)

def a3_clean(article_link):
    global sentence_threshold, debug, log
    html = ""
    try:
        html = html2text.html2text(open(article_link,"r",encoding='utf8').read().lower())
    except:
        print("HTML could not parse : "+article_link)
        log += "HTML could not parse : "+article_link+"\n"
        return (None,None)
    (data,broke) = filter_data(html)
    if broke:
        return (None,None)
    lines = list(map(lambda x : x.strip(),data.split("\n")))

    lines = list(filter(lambda x : True if len(x.split()) >= sentence_threshold else False,lines))

    #Text data is made here
    data = "\n".join(lines)

   #FIXME: for Nichit, nouns only
    lines = list(map(lambda x : " ".join(get_nouns(x)),lines))
    # clear empty lines
    lines = list(filter(lambda x : x != "",lines))
    nouns = "\n".join(lines)
    
    return (data,nouns)


root_folder_name = ".\\articles"
cleaned_folders = open(".\cleaned_folders.txt").read().split("\n")

if not debug:
    for folder_name in os.listdir(root_folder_name):
        #if folder not cleaned
        if folder_name not in cleaned_folders:
            print("In FOLDER : "+str(folder_name.upper()))
            print()
            texts,nouns = "",""
            for file_name in os.listdir(root_folder_name+"\\"+folder_name):
                print("In file : "+str(file_name))
                _text,_nouns = a3_clean(root_folder_name+"\\"+folder_name+"\\"+file_name)
                if _text != None:
                    texts += "\n"+_text
                    nouns += "\n"+_nouns
                    print("File Done : "+str(file_name))
                else:
                    print("File skipped : "+str(file_name))
                    log += "Skipped : "+str(file_name)+"\n"
            #open(".\\filtered_texts\\"+folder_name+".txt","w").write(texts)
            open(".\\filtered_nouns\\"+folder_name+".txt","w").write(nouns)
            print("FOLDER DONE : "+str(folder_name.upper()))
            print("\n")
            print("********************")
            print("\n")
            #add folders to cleaned_folder
            cleaned_folders.append(folder_name)
            #write cleaned_folders.txt
            open(".\cleaned_folders.txt","a").write(folder_name+"\n")
        #Else
        else:
            print("Folder Already cleaned : "+str(folder_name))
            print("\n")
            print("********************")
            print("\n")

    #write logs
    open("cleaning_logs.txt","w").write(log)
    #write non found
    data = "\n".join(not_found)
    open("not_found.txt","w",encoding='utf8').write(data)
    print("Wrote 'not found' list")
else:
    '''IF STUCK, TO DEBUG'''
    print(a3_clean("E:\Academics\Minor_Project\A5_overall\\articles\Construction\https_www_britannica_com_technology_construction_technology.html"))
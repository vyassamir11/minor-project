from html2text import html2text
import re,enchant,string,os
import numpy as np
from textblob import TextBlob
from collections import Counter
from nltk.corpus import stopwords
dictionary = enchant.Dict("en_US")

def get_nouns(text):
    if isinstance(text,str):
        blob = TextBlob(text)
    else:
        blob = TextBlob(" ".join(text))
    nouns = []
    for (word,tag) in blob.tags:
        if tag[0] == "N":
            nouns.append(word)
    return nouns

# data = open(".\\fish.html","r").read()
#
# out = html2text(data)
#
# allow = string.ascii_letters + string.digits + ' '
# data = re.sub('[^%s]' % allow,' ',data)
# data =re.sub('\d',' ',data)
# data = data.split()
# #consider only english words
# data_size = len(data)
# data = list(filter(lambda x : dictionary.check(x),data))
# data_english_text = " ".join(data)
# #stopword removal
# stop = set(stopwords.words('english'))
# clean_data = set(data) - stop
# clean_data = list(clean_data)
# #consider nouns
# clean_data_nouns = get_nouns(clean_data)
#
# print(data_english_text)

def PMI(category,article_link):
    nouns_lines = html2text(open(article_link,'r').read().lower()).split("\n")
    clean_data_nouns = []
    for _line in nouns_lines:
        clean_data_nouns.extend(_line.split(" "))
    # data_nouns = get_nouns(data)
    # headers_nouns = get_nouns(headers)

    # headers_count = Counter(headers_noun)
    # data_count = Counter(data_nouns)
    #data = data + " ".join(headers)
    # allow = string.ascii_letters + string.digits + ' '
    # data = re.sub('[^%s]' % allow,' ',data)
    # data =re.sub('\d',' ',data)
    # data = data.split()
    # #consider only english words
    # data_size = len(data)
    # data = list(filter(lambda x : dictionary.check(x),data))
    # data_english_text = " ".join(data)
    # #stopword removal
    # stop = set(stopwords.words('english'))
    # clean_data = set(data) - stop
    # clean_data = list(clean_data)
    #consider nouns
    # clean_data_nouns = get_nouns(clean_data)

    #PMI init
    clean_data_count = Counter(clean_data_nouns)
    # print(clean_data_count)
    nouns_prob = {}
    for ele in clean_data_nouns:
        #nouns_prob[ele] = clean_data_count[ele]/data_size
        nouns_prob[ele] = clean_data_count[ele]/len(clean_data_nouns)

    # If a category has more than 1 word in length then the highest occurring words
    # amongst them is considered as a representative
    # Make this generic
    # ind_name = category.split()[0].lower()
    if len(category.split()) > 0:
        contestants = []
        for word in category.lower().split():
            contestants.append((word,clean_data_count[word]))
        if max(contestants,key=lambda x : x[1])[0] == 0:
               print("Category name not found in input : ",category)
               ind_name = input("Please manually add a suitable one \n").strip().lower()
        else:
            ind_name = max(contestants,key=lambda x : x[1])[0]
    else:
        ind_name = category.lower()
    #ind_count = data_english_text.count(ind_name)
    ind_count = clean_data_count[ind_name]
    #ind_prob = ind_count/data_size
    ind_prob = ind_count/len(clean_data_nouns)

    # print(len(data))
    co_occur_prob = nouns_prob.copy() #initialize
    window = 15

    ind_name_split = ind_name.split()
    indices = np.where(np.array(data) == ind_name_split[0])

    for ele in clean_data_nouns:
        c = 0
        for index in indices[0]:
            # start = index-window if index>=window else 0
            end = index+window if index+window<len(data) else len(data)
            line = data[index:end]
            if ele in line:
                c += 1

        # co_occur_prob[ele] = c/data_size
        co_occur_prob[ele] = c/len(clean_data_nouns)

    pmi = co_occur_prob.copy()

    for ele in clean_data_nouns:
        pmi[ele] = np.log(co_occur_prob[ele]/(nouns_prob[ele]*ind_prob))

    out_list = list(map(lambda x : x[0],Counter(pmi).most_common(10)))
    return out_list

categories = os.listdir(".\\filtered_nouns")
data = ""
for _category in categories:
    print("In category ",_category[:_category.index(".")])
    out_list = PMI(_category[:_category.index(".")],str(".\\filtered_nouns\\"+_category))
    data += _category[:_category.index(".")]+"\n"
    data += ", ".join(out_list)+"\n"
    data += "\n"

open(".\out_PMI.txt","w").write(data)
print("Output file written")
import numpy as np
import os
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer

#Add list of industries 
#industry_list = ["food crop","cash crop","plantation crop","other crop","animal husbandry","forestry and logging","fishing","coal and lignite","natural gas","crude petroleum","iron ore","other minerals","sugar","food products excluding sugar","beverages","tobacco products","cotton textiles","wool, silk & synthetic fibre textiles","jute hemp and mesta textiles","textiles products including wearing apparel","wood and wood products except furniture","furniture and fixture","paper and paper products","printing, publishing and allied activities","leather and leather products","plastic and rubber products","petroleum products","coal tar products","inorganic heavy chemicals","organic heavy chemicals","fertilizers","paints, varnishes and lacquers","pesticides drugs and other chemicals","cement","non-metallic mineral products","iron and steel industries and foundries","other basic metal industry","metal products except mach and transport equipment","agricultural machinery","industrial machinery for food and textiles","other machinery","electronic machinery and appliances","railway transport equipment","other transport equipment","miscellaneous manufacturing industries","construction","electricity","water supply","railway transport services","other transport services","storage and warehousing","communication","trade","hotels and restaurants","banking","insurance","ownership of dwellings","education and research","medical and health","other services","public administration and defence"]
# list of text articles 
text = []
folder_name = ".\\filtered_nouns"
file_names = os.listdir(folder_name)
for f_name in file_names:
	lines = ""
	for line in open(folder_name+"\\"+f_name,'r').readlines():
		line = line.rstrip()
		lines = lines + " " + line
	text.append(lines)

# create the transform
vectorizer = CountVectorizer()
# tokenize and build vocab
vectorizer.fit(text)
# summarize
# print(vectorizer.vocabulary_)

# encode all articles (words)
vector = vectorizer.transform(text)

#Initializing list per industry which will contain the tags finally
tags_list = [[] for i in range(len(file_names))]


X = vector.toarray()
y = np.array(range(len(file_names))) # articles belong to which industry

clf = MultinomialNB()
clf.fit(X, y)
# MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True)

# Find max of each column in feature_log_prob_ 
# and check with a thershold to measure its significance
#print(np.exp(clf.feature_log_prob_))
all_feature_prob = clf.feature_log_prob_

#TODO: keep changing the threshold
threshold = np.log(0.0015)

for key in vectorizer.vocabulary_:
	# add the word with max prob to the list of that industry
	j = vectorizer.vocabulary_[key]
	i = np.argmax(all_feature_prob[:,j])
	if all_feature_prob[i][j] > threshold:
		tags_list[i].append(key)

data = ""
for i in range(len(file_names)):
	if tags_list[i] != []:
		data += file_names[i][:file_names[i].index(".txt")]+"\n"
		data += ", ".join(tags_list[i])+"\n"
		data += "\n"

open("out_NB.txt","w").write(data)
print("output file written!")
# print(tags_list)
# summarize encoded vector
# print(vector.shape)
# print(type(vector))
# print(vector.toarray())
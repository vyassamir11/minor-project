import os

delimiter = "\n"+"-"*15+"*"*20+"-"*15+"\n"
print(delimiter)
print("Cleaning articles and Extracting nouns")
os.system("python a5_clean.py")
print(delimiter)
print("Performing PMI")
os.system("python a2_PMI.py")
print(delimiter)
print("Performing Naive Bayes")
os.system("python a3_NB.py")
print(delimiter)
print("Performing LDA")
os.system("python a4_LDA.py")
print(delimiter)

# print("Getting Accuracy scores")
# os.system("python accuracy.py")
print(delimiter)
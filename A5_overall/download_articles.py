import requests
import os
import re

logs = ""

#downloads give url and keeps the stripped webpages as txt in .\article\$tag
def download_article(tag,urls):
    global logs
    file_names = []
    texts = []
    path = '.\\articles\\'+tag.strip()
    #if folder does not exist
    if not os.path.exists(path):
        os.makedirs(path)
    #download each url if can
    for url in urls:
        try:
            resp = requests.get(url)
        except:
            print('Could not retrieve '+url)
            logs += 'Could not retrieve '+"\n"+url+"\n\n"
            continue
        #replace not allowed file name characters from url with _
        file_path = re.sub('\W+','_',url)+'.html'
        open(path+'\\'+file_path,'w').write(str(resp.content))

#parse csv for urls
lines = open('.\\articles_links.csv','r').read().split('\n')
#remove empty strings corresponding to \n
#lines = [lines[i] for i in range(len(lines)) if lines[i] != '']
lines = list(filter(lambda x : x != '',lines))

for i in range(len(lines)):
    line_str = lines[i]
    #csv fields in row
    line = line_str.split(',')
    #if not already downloaded
    if line[-1] != 'd' and len(line) != 1:
        tag = line[0]
        download_article(tag,line[1:])
        line.append('d')
        #update the line
        lines[i] = ','.join(line)
    else:
        continue

#write the modified csv lines
open('.\\articles_links.csv','w').write('\n'.join(lines))
open(".\Downloading_Logs.txt","w").write(logs)
print("Downloaded the articles")
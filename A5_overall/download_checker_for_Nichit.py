import requests
from bs4 import BeautifulSoup
import re
import sys
import codecs

url = input("Enter URL \n")

html_page = requests.get(url).text
file_path = 'temp.html'
open(file_path,'w',encoding = 'utf-8').write(BeautifulSoup(html_page,"html.parser").prettify())
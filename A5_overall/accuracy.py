def read_out_file(link):
    """Returns a dictionary with categories and their tags"""
    dic = {}
    lines = open(link,'r').read().lower().split("\n")
    category,tags = None,None
    for i in range(len(lines)):
        if i%3 == 0:
            category = lines[i].strip()
        elif i%3 == 1:
            tags = list(map(lambda x : x.lower().strip(),lines[i].split(",")))
        else:
            dic[category] = tags
    return dic

lda_dic = read_out_file(".\out_LDA.txt")
pmi_dic = read_out_file(".\out_PMI.txt")
nb_dic = read_out_file(".\out_NB.txt")
manual_dic = read_out_file(".\out_manual.txt")

pmi_acc,nb_acc,lda_acc = 0,0,0

for category in manual_dic:
    manual_tags = set(manual_dic[category])
    pmi_tags,nb_tags,lda_tags = set(pmi_dic[category]),set(nb_dic[category]),set(lda_dic[category])
    # cat_pmi_acc = len(pmi_tags.intersection(manual_tags))/len(pmi_tags)
    # cat_nb_acc = len(nb_tags.intersection(manual_tags))/len(nb_tags)
    # cat_lda_acc = len(lda_tags.intersection(manual_tags))/len(lda_tags)
    pmi_acc += len(pmi_tags.intersection(manual_tags))/len(pmi_tags)
    nb_acc += len(nb_tags.intersection(manual_tags))/len(nb_tags)
    lda_acc += len(lda_tags.intersection(manual_tags))/len(lda_tags)

pmi_acc /= len(manual_dic)
nb_acc /= len(manual_dic)
lda_acc /= len(manual_dic)

print("Accuracy of PMI : "+str(pmi_acc))
print("Accuracy of NB : "+str(nb_acc))
print("Accuracy of LDA : "+str(lda_acc))


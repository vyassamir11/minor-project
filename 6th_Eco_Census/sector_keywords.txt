1
Food crops,Cash crops,Plantation crops,Other crops,Forestry & logging,fishing,Animal husbandry
wheat,rice,millets,millet,maize,legume,legumes,dal,cotton,cash crop,sugarcane,jute,tea,coffee,tobacco,groundnut,sesame,mustard,sunflower,castor,plantation,plantations,cashew,rubber,areca,palm,cocoa,coconut,spices,spice,tea,coffee,fruit,fruits,vegetables,vegetable,forestry,resin,lac,bark,paper,wood,timber,latex,fishing,seafood,fish,poultry,hatching,fisheries,nursury,milk,egg,dairy,livestock,seed,seeds,crops,poultry farming

2
Coal and lignite,Natural gas,Crude petroleum,Iron ore,Other minerals,Non Metalic Minerals,Iron & steel industries and foundries,Other basic metal industry,Metal products except mach
coal,lignite,bituminous,anthracite,peath,lng,cng,shale,methane,coalbed,natural gas,crude,hydrocarbons,asphalt,gasoline,barrels,diesel,iron,mangetite,hematite,limonite,pyrite,bauxite,chromite,ore,manganese,tungsten,dolomite,zircon,quartz,sandstone,clay,limestone,mica,feldspar,iron,steel,furnace,copper,brass,zinc,alloy,gold,silver,brass,steel,alloy,copper,quarry,mine,mines,mining,granite,stone,sand,salt

3
Sugar,Food products excluding sugar,Beverages,Tobacco products,Cotton textiles,Wool silk & synthetic fiber textiles,Jute,Textiles products including wearing apparel hemp and mesta textiles,Wood and wood products except furniture,Furniture and fixture,Paper and paper products,Printing publishing and allied activities,Leather and leather products,Plastic and rubber products,Petroleum products,Coal tar products,Miscellaneous manufacturing industries,Paints varnishes and Lacquers,Inorganic heavy chemicals,Organic heavy chemicals,Fertilizers,Pestisides
sugar,molasses,sucrose,khandsari,gur,jaggery,bakery,edible oil,dalda,beverage,beverages,juice,drink,drinks,alcohol,tobacco,gutka,cigarette,bidi,cigar,snuff,hooka,spinning,weaving,handloom,powerloom,yarn,wool,marino,polyester,nylon,acralic,spandex,rayon,silk,jute,hemp,mesta,fashion,cloth,fabric,garment,textile,clothing,lumber,plywood,timber,sawmill,furniture,carpentry,paper,paperboard,newsprint,writing paper,tissue paper,stationary,newspaper,book,card,bindery,envelope,leather,skins,tanning,hides,plastic,rubber,polyethylene,tire,latex,gasoline,petrol,diesel,lpg,lng,kerosene,wax,asphalt,petrochemicals,coal,tar,bitumen,peat,asphalt,graphite,watch,paint,lacquer,resin,varnish,pigment,binder,acid,base,salt,oxide,peroxide,rare gas,silicate,detergent,perfume,cosmetics,solvent,enzyme,dye,benzene,toluene,sweetner,fertilizer,pesticide,insecticide,jewellery,tailor,welding

4
Electricity
power,electricity,energy,transmission,vidyut,turbine

5
Water supply
mineral water,sanitation,purification,irrigation,bottling,drinking water,sewerage,filter,drainage,jal,pump,paani,water supply,water treatment,drinking water,rainwater harvesting,sewage

6
Construction,Cement
construction,cement,concrete,building,buildings,builder,contractor,public work,civil work,brick,bricks,demolish,demolition

7
Retailing and repairing services
grocery,shop,shops,retail,,retailing,sale,sales,supermarkets,repair,store,stores,workshop,repairing,suppliers,supplier,shopping

8
Railway transport services,Other transport services,Storage and warehousing
railway,train,rail,coach,passenger,route,express,line,railways,trains,coaches,road,bus,warehouse,storage,transport,transportation,shipping,cargo,freight,cold storage,logistics,courier,post,post office,flight,flights,seaways,airline,port,luggage,godown,station

9
Hotels and restaurants
hotel,hotels,resort,accommodation,restaurant,lodging,lodge,guest house,tiffin,mess,rest house

10
Communication
telecommunication,telephone,broadband,mobile,telecast,broadcast,telecom,radio,radio station,news,internet,broadcasting,telecasting,network,networks,cellular,cellular service,media,press,media house,sanchar,newspaper,wireless,software,information,information technology,IT,tech support,tech

11
Banking,Insurance,Trade
bank,banking,branch,loan,insurance,coverage,liability,trading,broker,stock,share,brokerage,trade,investment,investor,market,funds,shares,broker,finance,fund,loans,life insurance,lic,deposit,deposits,stock exchange,stocks,stock market,chitfund,credit,mutual fund,savings,saving

12
Ownership of dwellings
Estate,home,housing,apartment,land,building,real estate,property,properties,leasing,lease,rent,renting,flat,flats,hall,halls

13
Travel and Tourism, Professional Activities
Travel,tourism,tour,tours,travels,booking,bookings,ticketing,tickets,security service,security services,chartered accountant,chartered accountants,consultant,consultants,research,legal,lawyer,lawyers,photographer,photographers,photography,accounting,accountant,accountants,audit,auditing,audits,designing,designer,designers,lab,labs,laboratory,surveyor,surveyors,advocate,advocates,consultancy,bookkeeping

14
Administration activities
Administrative,administrate,administrates,office,auction,auctions,administration,packaging,packing,call centre,call center

16
Education and research
education,school,university,institutions,schools,institute,institutes,college,tuition,training,coaching,pathshala,pathashala,teaching

17
Medical and health
health,health care,hospital,medical treatment,x ray,social service,medical service,clinic,old age home,old age,health services,medical services,ayurvedic,nursing,allopathic,allopathy,ayurveda,ayurved,diagnostic center,diagnostic centre

18
Entertainment
Museum,library,water sports,scuba,theatre,theater,cinema,park,ground,stadium,club,entertainment,painting,drama,sports,zoo,dancing,band,gym,pool,games,game,multiplex,complex,amusement,gallery,carnival

19
Other service activities
Temple,mandir,masjid,church,gurudwara,ashram,devotion,religion,bhakti,spirituality,beauty parlour,mosque,spa,barber,dry cleaner,laundry,yoga,salon,fitness,rehabilitation
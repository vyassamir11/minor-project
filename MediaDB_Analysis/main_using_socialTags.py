from pymongo import MongoClient
from pandas import DataFrame
import time
import pandas
import sys

sector_tags = {}
districts = []
location = ['ProvinceOrState','City']
df = None
done = []

def read_tags_sectors_districts_progress(path_tags,path_districts):
    '''reads from tag file and district file  and progress file and creates dataframe'''
    global sector_tags,districts,df,done
    # read from tag file and make dictionary of sector:tags
    data = open(path_tags,'r').read().lower()
    lines = data.split("\n")
    temp_sector = None
    for i in range(len(lines)):
        if i%3 == 0:
            temp_sector = lines[i].strip()
        elif i%3 == 1:
            sector_tags[temp_sector] = [x.strip() for x in lines[i].split(",")]
        else:
            pass

    #read districts and remove empty lines
    districts = open(path_districts,"r").read().lower().split("\n")
    districts = list(map(lambda x : x.strip(),districts))
    try:
        districts.remove('')
    except:
        pass

    #read dataframe if existing else create new
    try:
        df = pandas.ExcelFile('out.xlsx').parse('Sheet1')
        sys.stderr.write("Read data frame from excel...\n")
        sys.stderr.flush()
    except:
        #create frame
        df = DataFrame(data=0,index=districts,columns=list(sector_tags.keys()))
        sys.stderr.write("Made frame and excel file...\n")
        sys.stderr.flush()
        df.to_excel('out.xlsx')

    #read sectors done if any else initialize to blank
    try:
        done = list(map(lambda x : x.strip(),open("sectors_done.txt","r").read().lower().split("\n")))
        sys.stderr.write("Initialized sectors done...\n")
        sys.stderr.flush()
    except:
        done = []
        sys.stderr.write("No previous sectors done...\n")
        sys.stderr.flush()

def update_frame(entities,sector):
    #increments cell value of district,sector if district is found as City or ProvinceOrState
    global districts,df,location
    for en in entities:
        if en['type'] in location:
            if en['name'].lower() in districts:
                df.loc[en['name'].lower(),sector] += 1

#read data and create variables
read_tags_sectors_districts_progress("out_manual.txt","districts.csv")

#connect to DB and get collection of articles
client = MongoClient('mongodb://10.237.26.154:27017')
articles = client['media-db']['articles']


for sector in list(sector_tags.keys())[:5]:
    #if sector already done
    if sector in done:
        s = time.time()
        sys.stderr.write("-----------------------------------------\n")
        sys.stderr.flush()
        sys.stderr.write("Beginning the sector :"+"\t"+sector+"\n")
        sys.stderr.flush()
        sys.stderr.write("Sector done already...")
        sys.stderr.flush()
        sys.stderr.write("Ended for the sector :"+"\t"+sector+"\n")
        sys.stderr.flush()
        t = time.time()
        sys.stderr.write("Took",t-s,"seconds"+"\n")
        sys.stderr.flush()
        sys.stderr.write("-----------------------------------------\n")
        sys.stderr.flush()
        continue

    sys.stderr.write("-----------------------------------------\n")
    sys.stderr.flush()
    s = time.time()
    sys.stderr.write("Beginning the sector :"+"\t"+sector+"\n")
    sys.stderr.flush()
    tag_list = sector_tags[sector]

    tag_regex = ''
    #make regex query string
    for tag in tag_list:
    #mongoDB regex uses \\s for space
    #single word tag like rice is always written in title case
        tag_regex += '^'+tag.title()+'$|'
    tag_regex = tag_regex[:-1]
    sys.stderr.write("Regex string here is : "+tag_regex+"\n")
    sys.stderr.flush()

    #fetch the articles
    sector_articles = list(articles.find({'socialTags.name':{'$regex': tag_regex}}))
    total_unused = len(sector_articles)
    sys.stderr.write("Total number of articles gathered : "+str(total_unused)+"\n")
    sys.stderr.flush()
    sys.stderr.write("Extracting locations from them...\n")
    sys.stderr.flush()
    total = 0
    for article in sector_articles:
        try:
            # the tag we're looking for must be relevant, importance should be 1
            for tag in article['socialTags']:
                if tag['importance'] == "1" and tag['name'].lower() in tag_list:
                    # if article has multiple relevant tags, it should be added multiple times
                    # a district can produce rice and wheat both
                    update_frame(article['entities'],sector)
                    total += 1
        except:
            sys.stderr.write("Found an article without entities : "+str(article['_id'])+"\n")
            sys.stderr.flush()
        # remove progress showing capabilities
        # if i%(total//10) == 0:
        #     sys.stderr.write(str(i//(total//10))+'...',end="")

    sys.stderr.write("\n")
    sys.stderr.flush()
    t = time.time()
    sys.stderr.write("Ended for the sector :"+"\t"+sector+"\n")
    sys.stderr.flush()
    sys.stderr.write("Took "+str(t-s)+" seconds\n")
    sys.stderr.flush()
    sys.stderr.write("-----------------------------------------\n")
    sys.stderr.flush()

    #add sector to done
    done.append(sector)

    #write done and excel
    open("sectors_done.txt","w").write("\n".join(done))
    df.to_excel('out.xlsx')

    print(sector,",",total,",",t-s)

from pymongo import MongoClient
from pandas import DataFrame
import re
import pandas
import sys

sector_tags = {}
sector_regexes = {}
districts = []
location = ['ProvinceOrState','City']
df = None

def read_tags_sectors_districts_regex(path_tags,path_districts):
    '''reads from tag file and district file  and progress file and creates dataframe'''
    global sector_tags,districts,df,done
    # read from tag file and make dictionary of sector:tags
    data = open(path_tags,'r').read().lower()
    lines = data.split("\n")
    temp_sector = None
    for i in range(len(lines)):
        if i%3 == 0:
            temp_sector = lines[i].strip()
        elif i%3 == 1:
            sector_tags[temp_sector] = [x.strip() for x in lines[i].split(",")]
        else:
            pass

    #read districts and remove empty lines
    districts = open(path_districts,"r").read().lower().split("\n")
    districts = list(map(lambda x : x.strip(),districts))
    try:
        districts.remove('')
    except:
        pass


    #create frame
    df = DataFrame(data=0,index=districts,columns=list(sector_tags.keys()))
    sys.stderr.write("Made frame and excel file...\n")
    sys.stderr.flush()
    df.to_excel('out.xlsx')

    for sector in sector_tags.keys():
        regex = ''
        for tag in sector_tags[sector]:
            regex += "^"+tag.title()+"|"+"\\s"+tag+",?\\s|"
        regex = regex[:-1]
        sector_regexes[sector] = regex

def update_frame(entities,sector):
    #increments cell value of district,sector if district is found as City or ProvinceOrState
    global districts,df,location
    for en in entities:
        if en['type'] in location:
            if en['name'].lower() in districts:
                df.loc[en['name'].lower(),sector] += 1

#read data and create variables
read_tags_sectors_districts_regex("out_manual.txt","districts.csv")

#connect to DB and get collection of articles
client = MongoClient('mongodb://10.237.26.154:27017')
articles = list(client['media-db']['samir_nichit_samples'].find())
total = len(articles)
print("Total articles : "+str(total))

i = 0
for article in articles:
    i += 1
    for sector in sector_regexes:
        if re.match(sector_regexes[sector],article['text']):
            try:
                update_frame(article['entities'],sector)
            except:
                pass
    if (i % (total //100) == 0):
        sys.stderr.write(str((i/total)*100)+"%  ")
        sys.stderr.flush()

df.loc['Total_in_sector'] = df.sum()
df['Total_in_district'] = df.sum(axis=1)
df.to_excel('out.xlsx')

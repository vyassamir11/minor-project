Vigilance officials seized a rice mill and arrested four persons for diverting the PDS rice into regular market at Banaganapalli in Kurnool district on Thursday. A truck, 170 bags of rice and car was also seized during the raid.

####################################

: The Commissionerate police, in a joint crackdown with civil supply department staff, busted a racket that involved illegal dealing of PDS rice. Five persons were arrested in this case.We have arrested three staff members of a model fair price shop (Maitree), a rice merchant of Mal Godown and a driver and forwarded them to court, said ACP Trinath Mishra here on Saturday.The police seized 195 packets, each containing 50 kg rice, and the vehicle used for transportation of the stuff.All the packets are marked as PDS rice meant to be distributed among BPL families at Re 1 per kg.The police said they intercepted the vehicle carrying some rice packets from a government model shop located at Buxibazaar to the godown of the rice merchant located at Mal Godown. Following the raids, the police seized more packets of PDS rice stored in the godown.After questioning the rice merchant and the staff of the model shop, it was revealed that the PDS rice meant for BPL families is clandestinely sold in the open market. It is a clear case of pilferage of subsidised rice and we believe a powerful racket is behind it, the ACP said.It is a clear case of pilferage of subsidised rice and we believe a powerful racket is behind it

####################################

In the name of drought, traders have hiked the prices of almost all essential commodities, including rice and tur dal.In a matter of two months, the price of Sona Masuri rice, which was being sold at around Rs. 30 a kg, has touched Rs. 38-Rs. 40 while the prices of tur, black gram and channa dals are up by Rs. 5 to Rs. 15 during the period.However, the saving grace is reduction in the prices of vegetablesPublic Eye finds out why.Mail your feedback to Public Eye, The Hindu , 19 & 21, Bhagwan Mahaveer Road (Infantry Road), Bangalore 560001 or publiceye.thehindu@gmail.com.

####################################

The Hassan district administration has set up centres in Hassan and Holenarsipur to procure levy rice from rice mill owners. The centres are located on the APMC premises in both the places. The price fixed by the State government for A-grade rice is Rs. 2,211.07 a quintal and for normal rice Rs. 2,162.53 a quintal. The government has intensified collection of levy rice after it launched Anna Bhagya scheme. Rice mill owners can contact the staff in-charge by calling 94498-88369 (Hassan) and 94492-11828 (Holenarsipur).

####################################

Based on the recommendation of T.H. Ganesh, Inspector of Police  Civil Supplies CID, District Collector Archana Patnaik on Friday ordered the detention of Selvam and Manikandan under Goondas Act on charges of smuggling rice meant for Public Distribution System.The two persons were in the habit of hoarding PDS rice, grinding it into flour, and smuggling it to the neighbouring State of Kerala.

####################################

Wheat arrival in the grain markets of Haryana during the current procurement season touched 48,76,910 tonnes.In comparison, only 9,44,520 tonnes of wheat had arrived in the mandis during the corresponding period last year, a spokesman of the States Food and Supplies Department said here on Tuesday. He said that the procurement process was running smoothly in the mandis of the State.The spokesman also said that out of the total arrival, 48,76,660 tonnes have been purchased by the government procurement agencies at Minimum Support Price.He said that more than 13.23 lakh tonnes has been procured by Food and Supplies Department.  PTI

####################################

Kendriya Bhandar opened its 96th consumer retail store in the Capital at Guru Gobind Singh Indraprastha University located in Dwarka.The grocery store of the Kendriya Bhandar carries a large variety of grocery and other consumer items and also offers a pleasant shopping ambience to the customers. The Bhandar is also offering laboratory-tested packed wheat flour at the special rates of Rs.182 per 10 kg bag, which is much lower than the price of dough in the market by over 20 per cent.A wide range of student stationary is available in the newly-launched store for the benefit of students.

####################################

Chandigarh: With yellow rust, a fungal disease, spotted in few fields, agriculture experts of Punjab Agricultural University (PAU) have asked wheat growers to be cautious and inspect their fields frequently. However, they also declared that they expected a good wheat crop this year due to pleasant weather.Experts advised that farmers should inspect their fields twice a week to detect the attack at an early stage, an official spokesperson said. Yellow rust has been reported in small areas in Ropar district.The agriculture department has said that if any farmer finds fungal infection in the form of yellow stripes on leaves or if the crop turns turmeric yellow, the farmers should first inform the agriculture department officials and apply the fungicide sprays recommended by PAU.After a recent survey conducted by experts of PAU, they told the farmers to apply fungicides such as Tilt 25 EC or Shine 25 EC or Bumper 25 EC in 200 litres of water per acre.

####################################

The Mysuru district administration seems to be facing a shortage of warehouses to store foodgrains procured this year.It has started procuring ragi and will buy maize from December 22.There are about 134 warehouses in the district of which 122 have stored the paddy procured one and half years ago. Most rice mill owners have not taken the paddy for hulling.After taking stock of the situation, Deputy Commissioner Shikha has set a one-day deadline for rice mill owners to take away the paddy. Chairing a special meeting here on Monday, Ms. Shikha said maize will be procured in all seven procurement centres set up across the district.

####################################

Kapurthala: The district police have busted a hang of wheat lifters by arresting its five members. The gang targeted foodgrain stocked in the godowns of government agencies. Kapurthala senior superintendent of police Rajinder Singh said that they had received a tip off that the accused, who had stolen wheat from a godown of Markfed in Deepewal village in a truck on April 15, 2015, were trying to target another godown."After their arrest and questioning it came out that they were also involved in taking away over 2,500 bags of wheat from four different godowns since April last. They would come in trucks in groups of over 20 and would then hurriedly load wheat from the godowns and speed away,"" the SSP said.
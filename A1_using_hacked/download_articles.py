import requests
import os
import re

#downloads give url and keeps the stripped webpages as txt in .\article\$tag
def download_article(tag,urls):
    file_names = []
    texts = []
    path = '.\\articles\\'+tag.strip()
    #if folder does not exist
    if not os.path.exists(path):
        os.makedirs(path)
    #download each url if can
    for url in urls:
        try:
            resp = requests.get(url)
        except:
            print('Could not retrieve '+url)
            continue
        #replace not allowed file name characters from url with _
        file_path = re.sub('\W+','_',url)+'.html'
        with open(path+'\\'+file_path,'w') as f:
            f.write(str(resp.content))

#parse csv for urls
with open('.\\articles_links.csv','r') as csv:
    lines = csv.read().split('\n')
    #remove empty strings corresponding to \n
    lines = [lines[i] for i in range(len(lines)) if lines[i] != '']

for i in range(len(lines)):
    line_str = lines[i]
    #csv fields in row
    line = line_str.split(',')
    #if not already downloaded
    if line[-1] != 'd':
        tag = line[0]
        download_article(tag,line[1:])
        line.append('d')
        #update the line
        lines[i] = ','.join(line)


with open('.\\articles_links.csv','w') as csv:
    #write the modified csv lines
    csv.write('\n'.join(lines))

print("Downloaded the articles")
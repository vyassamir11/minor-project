from rake_nltk import Rake
import os,re,pickle
from textblob import TextBlob
from nltk.corpus import stopwords

stop = set(stopwords.words('english'))

frequent_headers = pickle.load((open('.\\frequent_headers.bin','rb')))
nouns = pickle.load((open('.\\nouns.bin','rb')))

def remove_html_and_spec(string):
    return re.sub('\s+',' ',re.sub('\\\\r\\\\n',' ',re.sub('<[^>]*>',' ',string)))

def scavenge_articles_blob(tag,filter_words,file_name):
    global nouns,log,frequent_headers
    with open(file_name,'r',encoding='utf8') as f:
        data = f.read().lower()
        #remove recent press release from indian mirror
        #it is important to remove it before striping otherwise it could not be found
        if file_name.find('indianmirror') != -1:
            data = data[:data.find('recent press release')]
        bet_line_breaks = ' '.join(re.findall('<br>[^>]*<br>',data))
        bet_paras = ' '.join(re.findall('<p>[^>]*</p>',data))
        bet_headers = ' '.join(re.findall('<h[0-9]>[^>]*</h[0-9]>',data))
        bet_ol_headers = ' '.join(re.findall('<ol>[^>]*</ol>',data))
        bet_ul_headers = ' '.join(re.findall('<ul>[^>]*</ul>',data))
        bet_li = ' '.join(re.findall('<li>[^>]*</li>',data))
        bet_span_toctext = ' '.join(re.findall('<span class="toctext">[^>]*</span>',data))
        bet_strong = ' '.join(re.findall('<strong>[^>]*</strong>',data))

        data = ''
        data += remove_html_and_spec(bet_line_breaks)+" "
        data += remove_html_and_spec(bet_paras)+" "
        data += remove_html_and_spec(bet_li)
        #sentence check
        useful_sentences = []
        sentences = data.split('.')
        found_filter = 0
        for _sentence in sentences:
            for _filter in filter_words:
                if re.findall(_filter,_sentence) != []:
                    found_filter = 1
                    break
            if found_filter:
                useful_sentences.append(_sentence)
                found_filter = 0

        data = ".".join(useful_sentences)

        ''' data stop words removal'''
        clean_data = list(filter(lambda x : x not in stop,data.split(" ")))
        data = " ".join(clean_data)

        data_headers = remove_html_and_spec(bet_headers).split(" ")
        data_headers.extend(remove_html_and_spec(bet_ul_headers).split(" "))
        data_headers.extend(remove_html_and_spec(bet_ol_headers).split(" "))
        data_headers.extend(remove_html_and_spec(bet_strong).split(" "))
        data_headers.extend(remove_html_and_spec(bet_span_toctext).split(" "))
        nouns_data = [TextBlob(data).noun_phrases]
        print(type(nouns_data))
        print(nouns_data[0])
        #nouns_header = [TextBlob(data_headers).noun_phrases]
        # for word in data.split(" "):
        #     if word in nouns:
        #         nouns_data.append(word)
        # for word in data_headers:
        #     if word in nouns:
        #         nouns_header.append(word)

        # data = " ".join(nouns_data)
        # r = Rake()
        # r.extract_keywords_from_text(data)
        # tag_sentences = r.get_ranked_phrases()[:1]
        # nouns_header = set(nouns_header)
        # tag_headers = nouns_header.difference(frequent_headers)
        # try:
        #     tag_headers.remove('')
        # except:
        #     pass
        #
        #
        # print(tag_sentences)
        # print(tag_headers)

def scavenge_articles_nouns(tag,filter_words,file_name):
    global nouns,log,frequent_headers
    with open(file_name,'r',encoding='utf8') as f:
        data = f.read().lower()
        #remove recent press release from indian mirror
        #it is important to remove it before striping otherwise it could not be found
        if file_name.find('indianmirror') != -1:
            data = data[:data.find('recent press release')]
        bet_line_breaks = ' '.join(re.findall('<br>[^>]*<br>',data))
        bet_paras = ' '.join(re.findall('<p>[^>]*</p>',data))
        bet_headers = ' '.join(re.findall('<h[0-9]>[^>]*</h[0-9]>',data))
        bet_ol_headers = ' '.join(re.findall('<ol>[^>]*</ol>',data))
        bet_ul_headers = ' '.join(re.findall('<ul>[^>]*</ul>',data))
        bet_li = ' '.join(re.findall('<li>[^>]*</li>',data))
        bet_span_toctext = ' '.join(re.findall('<span class="toctext">[^>]*</span>',data))
        bet_strong = ' '.join(re.findall('<strong>[^>]*</strong>',data))

        data = ''
        data += remove_html_and_spec(bet_line_breaks)+" "
        data += remove_html_and_spec(bet_paras)+" "
        data += remove_html_and_spec(bet_li)
        #sentence check
        useful_sentences = []
        sentences = data.split('.')
        found_filter = 0
        for _sentence in sentences:
            for _filter in filter_words:
                if re.findall(_filter,_sentence) != []:
                    found_filter = 1
                    break
            if found_filter:
                useful_sentences.append(_sentence)
                found_filter = 0

        data = ".".join(useful_sentences)

        ''' data stop words removal'''
        clean_data = list(filter(lambda x : x not in stop,data.split(" ")))
        data = " ".join(clean_data)

        data_headers = remove_html_and_spec(bet_headers).split(" ")
        data_headers.extend(remove_html_and_spec(bet_ul_headers).split(" "))
        data_headers.extend(remove_html_and_spec(bet_ol_headers).split(" "))
        data_headers.extend(remove_html_and_spec(bet_strong).split(" "))
        data_headers.extend(remove_html_and_spec(bet_span_toctext).split(" "))

        nouns_data = []
        for word in data.split(" "):
            if word in nouns:
                nouns_data.append(word)

        print(nouns_data)
        # for word in data_headers:
        #     if word in nouns:
        #         nouns_header.append(word)

        # data = " ".join(nouns_data)
        # r = Rake()
        # r.extract_keywords_from_text(data)
        # tag_sentences = r.get_ranked_phrases()[:1]
        # nouns_header = set(nouns_header)
        # tag_headers = nouns_header.difference(frequent_headers)
        # try:
        #     tag_headers.remove('')
        # except:
        #     pass
        #
        #
        # print(tag_sentences)
        # print(tag_headers)

scavenge_articles_blob('Cash crops',['produces','produc','crop'],'.\sample.txt')
print()
scavenge_articles_nouns('Cash crops',['produces','produc','crop'],'.\sample.txt')

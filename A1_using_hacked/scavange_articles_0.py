from rake_nltk import Rake
import os
import re
from html.parser import HTMLParser

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_html(html):
    s = MLStripper()
    #take the body tag only
    html = ''.join(re.findall('<body[^>]*>.*</body>',html))
    s.feed(html)
    html = s.get_data()
    html = re.sub('\s+',' ',re.sub('\W',' ',re.sub("\\\\n"," ",re.sub('\\\\r'," ",html))))
    return html


def scavenge_article(tag):
    tag_list = []
    #for file_name in os.listdir(os.path.join('.\\articles',tag)):
    folder_name = '.\\articles\\'+tag
    for file_name in os.listdir(folder_name):
        with open(folder_name+'\\'+file_name,'r',encoding='utf8') as f:
            data = f.read().lower()
            #remove recent press release from indian mirror
            #it is important to remove it before striping otherwise it could not be found
            if file_name.find('indianmirror') != -1:
                data = data[:data.find('recent press release')]
            data = strip_html(data)
            data = re.findall('\s\w+\s',data)
            data = ' '.join(data)
            data = re.sub('\s+',' ',data)

            r = Rake()
            r.extract_keywords_from_text(data)
            tag_list.extend(r.get_ranked_phrases()[:5])
            tag_list = r.get_ranked_phrases()[:20]
            print(file_name)
            print(tag_list)

    #remove duplicates from tag list
    tag_list = list(map(lambda x : x.split(' '),tag_list))
    flat_tag_list = [item for sublist in tag_list for item in sublist]
    return list(set(flat_tag_list))


with open('.\\articles_tags.csv','r') as csv:
    lines = csv.read().split('\n')
    #remove empty strings corresponding to \n
    lines = [lines[i] for i in range(len(lines)) if lines[i] != '']

for i in range(len(lines)):
    line_str = lines[i]
    #csv fields in row
    line = line_str.split(',')
    #if not already scavenged
    if line[-1] != 's':
        tag = line[0]
        tag_list = scavenge_article(tag)
        #extend the line with tags
        line.extend(tag_list)
        line.append('s')
        #update the line
        lines[i] = ','.join(line)

with open('.\\articles_tags.csv','w') as csv:
    #write csv with updated lines
    csv.write('\n'.join(lines))

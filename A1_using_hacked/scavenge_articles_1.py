from rake_nltk import Rake
import os
import re,pickle
from textblob import TextBlob

frequent_headers = pickle.load((open('.\\frequent_headers.bin','rb')))
log = ''
nouns = pickle.load(open('.\\nouns.bin','rb'))
def remove_html_and_spec(string):
    return re.sub('\s+',' ',re.sub('\\\\r\\\\n',' ',re.sub('<[^>]*>',' ',string)))

def get_data_and_headers(data):
    bet_line_breaks = ' '.join(re.findall('<br>[^>]*<br>',data))
    bet_paras = ' '.join(re.findall('<p>[^>]*</p>',data))
    bet_headers = ' '.join(re.findall('<h[0-9]>[^>]*</h[0-9]>',data))
    bet_ol_headers = ' '.join(re.findall('<ol>[^>]*</ol>',data))
    bet_ul_headers = ' '.join(re.findall('<ul>[^>]*</ul>',data))
    bet_li = ' '.join(re.findall('<li>[^>]*</li>',data))
    bet_span_toctext = ' '.join(re.findall('<span class="toctext">[^>]*</span>',data))
    bet_strong = ' '.join(re.findall('<strong>[^>]*</strong>',data))

    data = ''
    data += remove_html_and_spec(bet_line_breaks)+" "
    data += remove_html_and_spec(bet_paras)+" "
    data += remove_html_and_spec(bet_li)

    headers = []
    headers.extend(remove_html_and_spec(bet_headers).split())
    headers.extend(remove_html_and_spec(bet_ol_headers).split())
    headers.extend(remove_html_and_spec(bet_ul_headers).split())
    headers.extend(remove_html_and_spec(bet_span_toctext).split())
    headers.extend(remove_html_and_spec(bet_strong).split())

    return (data,headers)

def get_nouns(text):
    if isinstance(text,str):
        blob = TextBlob(text)
    else:
        blob = TextBlob(" ".join(text))
    nouns = []
    for (word,tag) in blob.tags:
        if tag[0] == "N":
            nouns.append(word)
    return nouns

def scavenge_articles(tag,filter_words):
    global nouns,log,frequent_headers
    tag_list = []
    header_list = []
    folder_name = '.\\articles\\'+tag
    for file_name in os.listdir(folder_name):
        with open(folder_name+'\\'+file_name,'r',encoding='utf8') as f:
            data = f.read().lower()
            #remove recent press release from indian mirror
            #it is important to remove it before striping otherwise it could not be found
            if file_name.find('indianmirror') != -1:
                data = data[:data.find('recent press release')]
            # bet_line_breaks = ' '.join(re.findall('<br>[^>]*<br>',data))
            # bet_paras = ' '.join(re.findall('<p>[^>]*</p>',data))
            # bet_headers = ' '.join(re.findall('<h[0-9]>[^>]*</h[0-9]>',data))
            # bet_ol_headers = ' '.join(re.findall('<ol>[^>]*</ol>',data))
            # bet_ul_headers = ' '.join(re.findall('<ul>[^>]*</ul>',data))
            # bet_li = ' '.join(re.findall('<li>[^>]*</li>',data))
            # bet_span_toctext = ' '.join(re.findall('<span class="toctext">[^>]*</span>',data))
            # bet_strong = ' '.join(re.findall('<strong>[^>]*</strong>',data))

            # data = ''
            # data += remove_html_and_spec(bet_line_breaks)+" "
            # data += remove_html_and_spec(bet_paras)+" "
            # data += remove_html_and_spec(bet_li)

            data,data_headers = get_data_and_headers(data)
            #sentence check
            useful_sentences = []
            sentences = data.split('.')
            found_filter = 0
            for _sentence in sentences:
                for _filter in filter_words:
                    if re.findall(_filter,_sentence) != []:
                        found_filter = 1
                        break
                if found_filter:
                    useful_sentences.append(_sentence)
                    found_filter = 0

            data = ".".join(useful_sentences)

            # data_headers = remove_html_and_spec(bet_headers).split(" ")
            # data_headers.extend(remove_html_and_spec(bet_ul_headers).split(" "))
            # data_headers.extend(remove_html_and_spec(bet_ol_headers).split(" "))
            # data_headers.extend(remove_html_and_spec(bet_strong).split(" "))
            # data_headers.extend(remove_html_and_spec(bet_span_toctext).split(" "))
            # nouns_data = []
            # nouns_header = []
            # for word in data.split(" "):
            #     if word in nouns:
            #         nouns_data.append(word)
            # for word in data_headers:
            #     if word in nouns:
            #         nouns_header.append(word)

            nouns_data = get_nouns(data)
            nouns_header = get_nouns(data_headers)
            
            data = " ".join(nouns_data)
            r = Rake()
            r.extract_keywords_from_text(data)
            tag_sentences = r.get_ranked_phrases()[:1]
            nouns_header = set(nouns_header)
            tag_headers = nouns_header.difference(frequent_headers)
            try:
                tag_headers.remove('')
            except:
                pass
            # log += tag+"\n"
            # log += file_name+"\n"
            # log += str(tag_sentences)+"\n"
            # log += str(tag_headers)+"\n"
            
            tag_list.append(tag_sentences.split(" "))
            header_list.append(list(tag_headers))

    header_list.extend(tag_list)
    flatten_out_list = [ele for sub_list in header_list for ele in sub_list]
    return flatten_out_list

with open('.\\articles_tags.csv','r') as csv:
    lines = csv.read().split('\n')
    #remove empty strings corresponding to \n
    lines = [lines[i] for i in range(len(lines)) if lines[i] != '']

with open('.\\articles_filters.csv','r') as f:
    filter_lines = f.read().split("\n")
    #remove empty strings corresponding to \n
    filter_lines = [filter_lines[i] for i in range(len(filter_lines)) if filter_lines[i] != '']

for i in range(len(lines)):
    line_str,filter_line_str = lines[i],filter_lines[i]
    #csv fields in row
    line,filter_line = line_str.split(','),filter_line_str.split(',')
    #if not already scavenged
    if line[-1] != 's':
        tag,filters = line[0],filter_line[1:]
        tag_list = scavenge_articles(tag,filters)
        #extend the line with tags
        line.extend(tag_list)
        line.append('s')
        #update the line
        lines[i] = ','.join(line)


with open('.\\articles_tags.csv','w') as csv:
    #write csv with updated lines
    csv.write('\n'.join(lines))

with open('.\scavenge_log.txt','a') as f:
    f.write(log)

print('Tags are extracted and log is written')
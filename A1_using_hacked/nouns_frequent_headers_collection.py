from nltk.corpus import wordnet as wn
import pickle

nouns = {x.name().split('.',1)[0] for x in wn.all_synsets('n')}

#add indian names to nouns
indian_names = ['uttar','pradesh','madhya','punjab','bihar','gujarat','bengal','haryana','maharashtra','andrha','telangana','assam','delhi']
for _inidian_name in indian_names:
    nouns.add(_inidian_name)

pickle.dump(nouns,open('nouns.bin','wb'))
print('Nouns written in file')

frequent_headers = set(['operation','capacity','country','district','industry','variety','high','fire','stand','modern','future','statistic','specific','import','phase','generation','system','growth','effects','purpose','diversity','links','external','see','production','operation','fund','development','infrastructure','capital','strengthening','marketing','venture','quality','cultivation','post','strategy','history','law','regulations','economics','economic','mafia','more','health','construction','classification','consumption','meaning','current','satuts','etymology','commercial','purpose','diversity','links','','distribution','benefits','futher','reading','citations','management','research','training','issues','menu','navigation','contents','year','list','home','india','gallery','about','us','contanct','articles','privacy','policy','advertise','follow','contact','with','related'])
pickle.dump(frequent_headers,open('frequent_headers.bin','wb'))
print('Frequent headers written in file')
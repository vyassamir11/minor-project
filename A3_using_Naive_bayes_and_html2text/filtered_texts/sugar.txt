Today India has 453 sugar mills those constituting 252 mills from the Co
Uttar Pradesh Maharashtra Karnataka Gujarat Tamil Nadu and Andhra Pradesh
India is the largest consumer of sugar and consumes around 16 million tonnes
Sugar is made from sugarcane and was discovered thousands of years ago
in New Guinea. And then the route was traced to India and Southeast Asia.
India was the first to begin with the production of sugar following the
process of pressing sugarcane to extract juice and boil it to get crystals.
The government of India in 195051 made serious industrial development plans
and has set many targets for production and consumption of sugar. These plans
by the government projected the license and installment capacity for the sugar
industry in its Five Year Plans. India is well known as the original home of
Indian mythology supports the fact it contains legends showing the
origin of sugarcane. Today India is the second largest producer of sugarcane
next to Brazil. Currently there are about 4 million hectares of land under
sugarcane with an average yield of 70 tonnes per hectare.
India is the largest producer of sugar including traditional cane sugar
followed by Brazil in the second place at 185 million tonnes. Even in respect
of white crystal sugar India has ranked position in 7 out of last 10
mostly by the rural population in the country. In the early nearly
23rd of sugarcane production was used for the production of alternate
of living and higher incomes the sweetener demand has shifted to white sugar.
In the year 1930 there was an advent of modern sugar processing industry in
India which was started with grant of tariff protection to the sugar industry.
In the year 193031 the number of sugar mills increased from 30 to 135 and in
the year 193536 production was increased from 120 tonnes to 934
tonnes under the dynamic leadership of the private sector. In the year 195051
the era of planning for industrial development began and Government laid down
targets of sugar production and consumption licensed and installed capacity
sugarcane production during each of the Five Year Plan periods
India is the largest sugar consumer and second largest producer of sugar in
the world according to the USDA Foreign Agricultural Service. Indian Sugar
Industry has total turnover of Rs. 500 billion per annum and contributes
almost Rs. 225 billion to central and state exchequer as tax and
excise duty every year according to the sources of Ministry of Food
Sugar Industry is regarded second after the Textile Industry in India as per
the industry in the country. The industry currently has 453
operating sugar mills in different parts of the country. Indian sugar industry
has always been a focal point for socioeconomic development in the rural
areas. Today nearly 50 million sugarcane farmers and a large number of
Indian Sugar Industry generates power for its own requirement and even gets
surplus power for export to the grid based on byproduct There is even
production of ethanol an ecology friendly and renewable energy for blending
with petrol. Sugar Companies have been established in large sugarcane growing
states like Uttar Pradesh Maharashtra Karnataka Gujarat Tamil Nadu and
Andhra Pradesh and are the six states contributing more than 85 of total
sugar production in the India. And 57 of total production is together
contributed by Uttar Pradesh and Maharashtra. Indian sugar industry has been
growing horizontally with large number of small sized sugar plants set up
throughout India as opposed to the consolidation of capacity in the rest of
the important sugar producing countries and sellers of sugar where there is
The Sugar industry In India has two sectors including organized and
unorganized sector. The Sugar factories usually belong to the organized sector
and those producers who produce traditional sweeteners fall into unorganized
Taking raw sugar to a refinery for the process of filtering and washing to discard remaining elements and hue
Today India has 453 sugar mills those constituting 252 mills from the Co
operative sector and 134 Mills from the private sector. And there are boosting
67 mills in the Public sector. As according to the statistics there is total
number of 571 sugar factories in India as on March 31 2005 compared to 138
during 1950-51. These 571 sugar mills have a production of total quantity of
192 million tonnes There is an increase in the Sugar production in
India from 155 MT in 199899 to 201 MT in 200203
opportunities in rural India. Today the Indian Sugar Industry has absorbed
about 5 rural people. The cultivation of sugarcane employs about 45 core
farmers which is the first phase of the sugar production. Indian Sugar mills
may be cooperatives public or private enterprises. The industry today
provides employment to about 2 million skilled workers and others
People interested for Jobs in Sugar Industry should be a diploma holder in
Mechanical or electrical engineering along with the diploma in the sugar
technology. At the entry level one can expect a remuneration of Rs 60007000
per month and as with experience increases the pay may go up to Rs 615
per annum. Sugar technologists are usually employed in sugar factories
alcoholic or non alcoholic production plants or sugar research labs. The
major recruiters in the sugar Industry are and National Federation of
The Indian Sugar Industry comes under the classification of Red which
is successful in capturing new markets with strategic advantage like
the first time Indian white sugar was marketed by for direct consumption
in Europe while in the past Indian sugar shipped to Europe was diverted to
other destinations. In the year 2000 10000 MT was exported in containers for
The following policy initiatives are taken to boost the Sugar industry
Government declared the new policy on August 201998 with regards to licenses for new factories which shows that there will be no other sugar factory in a radius of 15 km.
Setting up of Indian Institute of Sugar Technology at Kanpur is meant for improving efficiency in the industry.
In the year 1982 the sugar development fund was set up with a view to avail loans for modernization of the industry.
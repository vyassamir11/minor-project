import re,enchant,html2text,os
from textblob import TextBlob

dictionary = enchant.Dict("en_US")
#FIXME: add non trivial words to Dictionary
non_trivial = open("nontrivial.txt","r").readlines()
for _line in non_trivial:
    dictionary.add(_line.strip())

def get_nouns(text):
    if isinstance(text,str):
        blob = TextBlob(text)
    else:
        blob = TextBlob(" ".join(text))
    nouns = []
    for (word,tag) in blob.tags:
        if tag[0] == "N":
            nouns.append(word)
    return nouns


def clean_word(word):
    #reomves punctuations from a word, ignores weirdly Unicode chars like Ã,È, etc
    word = word.encode().decode('unicode_escape')
    #end of sentence word, useful for further analysis
    if word[-1] == ".":
        return word
    return re.sub("\W+","",word)


def filter_data(data):
    #gives legit words from data, preserves the lines
    out,start = "",0
    while(data.find("\n",start) != -1):
        end = data.index("\n",start)
        line = data[start:end].split()
        #clear all the words of line
        line = list(map(clean_word,line))
        #ensure line does not have any empty strings
        line = list(filter(lambda x : x != "",line))
        new_line = list(filter(lambda x : dictionary.check(x),line))
        out += " ".join(new_line)+"\n"
        start = end+1
    #uniform line gap
    out = re.sub("\n+",r"\n",out)
    return out

def a3(article_link):
    data = filter_data(html2text.html2text(open(article_link,"r").read()))

    #now consider only those lines which end with dot, i.e. are sentences
    lines = data.split("\n")
    lines = list(filter(lambda x : x[-1] == "." if len(x) > 0 else False,lines))

    data = "\n".join(lines)

    return data

    # r = Rake()
    # r.extract_keywords_from_sentences(data)
    # print(r.get_ranked_phrases())

f_names = os.listdir(".\\articles\\")
for i in range(len(f_names)):
    open(".\\filtered_texts\\"+f_names[i][:f_names[i].find(".")]+".txt","w").write(a3(".\\articles\\"+f_names[i]))
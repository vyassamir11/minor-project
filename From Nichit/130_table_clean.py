with open('130_table.txt','r') as f:
    data = f.read()

out = ''
for i in range(len(data)-1):
    char = data[i]
    if (char == '\r\n' or char == '\n') and (data[i+1] not in ['1','0']):
        out += ' '
    else:
        out += char
#print(out)
with open('cleaned_130_tables.txt','w') as f:
    f.write(out)
print('Manually clean IoT and Page numbers now')

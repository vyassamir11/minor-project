with open('cleaned_130_tables.txt','r') as f:
    data = f.readlines()

out = ''
for line in data:
    line_l = line.split(" ")
    out += line_l[0]+'\t'
    line_ll = " ".join(line_l[1:]).split(';')
    assert (len(line_ll) ==  2),'something wrong'+str(line_ll)
    out += line_ll[0]+"\t"+line_ll[1]
    
#print(out)
with open('130_table.tsv','w') as f:
    f.write(out)

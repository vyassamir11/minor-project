from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import requests
import re
from html.parser import HTMLParser
import math
from textblob import TextBlob as tb

tag = 'cement'
#file_name = 'raw_'+tag
urls = ['https://en.wikipedia.org/wiki/Iron_and_steel_industry_in_India','https://www.importantindia.com/11174/iron-and-steel-industry-in-india/']
contents = []
for url in urls:
    resp = requests.get(url)
    contents.append(str(resp.content))

#os.system('python .\stem.py '+file_name+' '+file_name)


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

def stem(sentence):
    sentence = " ".join(re.findall("\w+",sentence))
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(sentence)
    filtered_sentence = []
    for w in word_tokens:
        if w not in stop_words:
            filtered_sentence.append(w)

    return " ".join(filtered_sentence)



docs = [strip_tags(content) for content in contents]
docs = list(map(lambda a : "".join(re.findall('\s\w+\s',a)).strip(),docs))
docs = list(map(lambda b : re.sub("\s+"," ",b).lower(),docs))
docs = list(map(lambda b : stem(b),docs))

print(docs[0])
print(docs[1])

def tf(word, blob):
    return blob.words.count(word) / len(blob.words)

def n_containing(word, bloblist):
    return sum(1 for blob in bloblist if word in blob.words)

def idf(word, bloblist):
    return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))

def tfidf(word, blob, bloblist):
    return tf(word, blob) * idf(word, bloblist)

tb_docs = [tb(doc) for doc in docs]
bloblist = list(tb_docs)
for i, blob in enumerate(bloblist):
    print("Top words in document {}".format(i + 1))
    scores = {word: tfidf(word, blob, bloblist) for word in blob.words}
    sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
    for word, score in sorted_words[:3]:
        print("\tWord: {}, TF-IDF: {}".format(word, round(score, 5)))


with open('60_table.txt','r') as f:
    data = f.readlines()

out = ''
for line in data:
    num = line.split(" ")[0]
    out += num+'\t' 
    line = line[len(num)+1:]
    start = line.index('.')
    end = line.rindex('.')
    out += line[:start]+"\t"+line[end+1:]
    
#print(out)
with open ('60_tables.tsv','w') as f:
    f.write(out)
    

import re,os
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string
# Importing Gensim
import gensim
from gensim import corpora
stop = set(stopwords.words('english'))
exclude = set(string.punctuation)
lemma = WordNetLemmatizer()
#read custom stop words and add them to the stop set
custom_stop = set(list(map(lambda x : x.strip(),list(open("./custom_stop.txt","r").read().split("\n")))))
stop = stop.union(custom_stop)

def clean(doc):
    #cleans a line
    global stop,lemma,exclude
    #important to remove punctuations embedded in a word
    stop_free = " ".join([i for i in doc.split() if re.sub("\W","",i) not in stop])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    return normalized


def divide(s,n):
    #divides article text s into n chunks of equal lines
    out = []
    #if no of lines in articles <=n then chunk
    if len(s.split("\n")) <= n:
        print("Number of lines is less than no of chunks")
        s_words = s.split(" ")
        chunk_size = len(s_words)//n
        for i in range(chunk_size):
            out.append(" ".join(s_words[i*chunk_size:(i+1)*chunk_size]))
        return out
    #else
    else:
        s_lines = s.split("\n")
        chunk_size = len(s_lines)//n
        for i in range(chunk_size):
            out.append("\n".join(s_lines[i*chunk_size:(i+1)*chunk_size]))
        return out

def clean_part(part):
    #clean a paragraph part, uses function clean to clean a line
    lines = part.split("\n")
    if len(lines) == 1:#if only one line
        return clean(part)
    else:
        lines = list(map(clean,lines))
        return "\n".join(lines)

def run_lda(doc_complete):
    global stop,exclude,lemma

    #doc_clean = [clean(doc).split() for doc in doc_complete]

    #divide doc_complete in 4 equal sized chunks and clean lines of chunks
    parts = divide(doc_complete,8)
    doc_clean = list(map(clean_part,parts))
    doc_clean_words = [doc.split() for doc in doc_clean]

    # Creating the term dictionary of our courpus, where every unique term is assigned an index. dictionary = corpora.Dictionary(doc_clean)
    dictionary = corpora.Dictionary(doc_clean_words)
    # Converting list of documents (corpus) into Document Term Matrix using dictionary prepared above.
    doc_term_matrix = [dictionary.doc2bow(doc) for doc in doc_clean_words]
    # Creating the object for LDA model using gensim library
    Lda = gensim.models.ldamodel.LdaModel

    # Running and Training LDA model on the document term matrix.
    ldamodel = Lda(doc_term_matrix, num_topics=1, id2word = dictionary, passes=50)


    #'0.035*"pulse" + 0.031*"bean" + 0.027*"rice" + 0.022*"cultivation" + 0.017*"pea" + 0.016*"wheat" + 0.014*"maize" + 0.012*"variety"'
    #such string is received in s, it is hidden in a single entity list's tuple's second value
    #I convert it into list of keywords
    s = ldamodel.print_topics(num_topics=1, num_words=8)[0][1]
    #it always has a number in its front, skipping that with 1:
    out_l = list(map(lambda x : x[x.find('"')+1:x.rfind('"')],s.split("*")))[1:]
    return out_l

folder_name = "filtered_nouns"
file_names = os.listdir(folder_name)
doc_complete = []
out_data = ""
print("Performing LDA on : "+folder_name.upper())
for file_name in file_names:
    tags = run_lda(open(folder_name+"\\"+file_name,"r").read().lower())
    out_data += file_name[:file_name.index(".")]+"\n"+", ".join(tags)+"\n"
    out_data += "\n"
    print("Category Done : "+file_name[:file_name.index(".txt")])

if folder_name == "filtered_nouns":
    open('out_using_nouns.txt','w').write(out_data)
if folder_name == "filtered_texts":
    open('out_using_texts.txt','w').write(out_data)

print("LDA performed")
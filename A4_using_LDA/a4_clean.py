import re,enchant,html2text,os
from textblob import TextBlob

sentence_threshold = 10
not_found = []
dictionary = enchant.Dict("en_US")
#FIXME: add non trivial words to Dictionary
non_trivial = open("nontrivial.txt","r").read().split("\n")
for _line in non_trivial:
    dictionary.add(_line.strip().lower())
custom_stop = set(open("custom_stop.txt","r").read().lower().split("\n"))


def get_nouns(text):
    if isinstance(text,str):
        blob = TextBlob(text)
    else:
        blob = TextBlob(" ".join(text))
    nouns = []
    for (word,tag) in blob.tags:
        if tag[0] == "N":
            nouns.append(word)
    return nouns


def clean_word(word):
    #reomves punctuations from a word, ignores weirdly Unicode chars like Ã,È, etc
    try:
        word = word.encode().decode('unicode_escape')
    except:
        print("Codec exception for : "+str(word))
    #end of sentence word, useful for further analysis
    if word[-1] == ".":
        return word
    return re.sub("\W+","",word)


def filter_data(data):
    global not_found,counter_not_found,counter_threshold
    #gives legit words from data, preserves the lines
    out,start = "",0
    while(data.find("\n",start) != -1):
        end = data.index("\n",start)
        line = data[start:end].split()
        #clear all the words of line
        line = list(map(clean_word,line))
        #ensure line does not have any empty strings
        line = list(filter(lambda x : x != "",line))
        #new_line = list(filter(lambda x : dictionary.check(x),line))
        new_line = []
        for _word in line:
            if dictionary.check(_word) and (_word not in custom_stop):
                new_line.append(_word)
            else:
                if _word in custom_stop:
                    not_found.append(_word)

        out += " ".join(new_line)+"\n"
        start = end+1
    #uniform line gap
    out = re.sub("\n+",r"\n",out)
    return out

def a3_clean(article_link):
    global sentence_threshold
    data = filter_data(html2text.html2text(open(article_link,"r",encoding='utf8').read().lower()))

    lines = data.split("\n")

    lines = list(filter(lambda x : True if len(x.split()) >= sentence_threshold else False,lines))

    #Text data is made here
    data = "\n".join(lines)

   #FIXME: for Nichit, nouns only
    lines = list(map(lambda x : " ".join(get_nouns(x)),lines))
    nouns = "\n".join(lines)
    
    return (data,nouns)


root_folder_name = ".\\articles"
cleaned_folders = open(".\cleaned_folders.txt").read().split("\n")

for folder_name in os.listdir(root_folder_name):
    #if folder not cleaned
    if folder_name not in cleaned_folders:
        print("In FOLDER : "+str(folder_name.upper()))
        print()
        texts,nouns = "",""
        for file_name in os.listdir(root_folder_name+"\\"+folder_name):
            print("In file : "+str(file_name))
            _text,_nouns = a3_clean(root_folder_name+"\\"+folder_name+"\\"+file_name)
            texts += "\n"+_text
            nouns += "\n"+_nouns
            print("File Done : "+str(file_name))
        open(".\\filtered_texts\\"+folder_name+".txt","w").write(texts)
        open(".\\filtered_nouns\\"+folder_name+".txt","w").write(nouns)
        print("FOLDER DONE : "+str(folder_name.upper()))
        print("\n")
        print("********************")
        print("\n")
        #add folders to cleaned_folder
        cleaned_folders.append(folder_name)
        #write cleaned_folders.txt
        open(".\cleaned_folders.txt","a").write(folder_name+"\n")
    #Else
    else:
        print("Folder Already cleaned : "+str(folder_name))
        print("\n")
        print("********************")
        print("\n")

#write non found
data = "\n".join(not_found)
open("not_found.txt","w",encoding='utf8').write(data)
print("Wrote not found list")

'''IF STUCK, TO DEBUG'''
#a3_clean("")
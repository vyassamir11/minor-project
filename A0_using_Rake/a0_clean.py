import re,enchant,html2text,os
from textblob import TextBlob

#Although it is used here, it was developed in A3

sentence_threshold = 10
not_found = []
dictionary = enchant.Dict("en_US")
#FIXME: add non trivial words to Dictionary
non_trivial = open("nontrivial.txt","r").readlines()
for _line in non_trivial:
    dictionary.add(_line.strip())

def get_nouns(text):
    if isinstance(text,str):
        blob = TextBlob(text)
    else:
        blob = TextBlob(" ".join(text))
    nouns = []
    for (word,tag) in blob.tags:
        if tag[0] == "N":
            nouns.append(word)
    return nouns


def clean_word(word):
    #reomves punctuations from a word, ignores weirdly Unicode chars like Ã,È, etc
    try:
        word = word.encode().decode('unicode_escape')
    except:
        print("Codec exception for : "+str(word))
    #end of sentence word, useful for further analysis
    if word[-1] == ".":
        return word
    return re.sub("\W+","",word)


def filter_data(data):
    global not_found
    #gives legit words from data, preserves the lines
    out,start = "",0
    while(data.find("\n",start) != -1):
        end = data.index("\n",start)
        line = data[start:end].split()
        #clear all the words of line
        line = list(map(clean_word,line))
        #ensure line does not have any empty strings
        line = list(filter(lambda x : x != "",line))
        #new_line = list(filter(lambda x : dictionary.check(x),line))
        new_line = []
        for _word in line:
            if dictionary.check(_word):
                new_line.append(_word)
            else:
                not_found.append(_word)
        out += " ".join(new_line)+"\n"
        start = end+1
    #uniform line gap
    out = re.sub("\n+",r"\n",out)
    return out

def a3(article_link):
    global sentence_threshold
    data = filter_data(html2text.html2text(open(article_link,"r").read()))

    #now consider only those lines which end with dot or space, i.e. are sentences
    lines = data.split("\n")
    #lines = list(filter(lambda x : True if (x[-1] == "." or x[-1] == " ") else False,lines))
    lines = list(filter(lambda x : True if len(x.split()) >= sentence_threshold else False,lines))

    #Text data is made here
    data = "\n".join(lines)

#    #FIXME: for Nichit, nouns only
#     lines = list(map(lambda x : " ".join(get_nouns(x)),lines))
#     nouns = "\n".join(lines)
    
    return data

    # r = Rake()
    # r.extract_keywords_from_sentences(data)
    # print(r.get_ranked_phrases())

folder_name = ".\\samples"
file_names = os.listdir(folder_name)
for i in range(len(file_names)):
    text = a3(folder_name+"\\"+file_names[i])
    open(".\\filtered_texts\\"+file_names[i][:file_names[i].find(".")]+".txt","w").write(text)
    

data = ""
for _not_word in not_found:
    data += _not_word+"\n"
open("not_found.txt","w",encoding='utf8').write(data)
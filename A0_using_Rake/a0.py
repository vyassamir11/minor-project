from rake_nltk import Rake
import os

def extract_kw(link):
    r = Rake()
    text = open(link,"r").read()
    r.extract_keywords_from_text(text)
    return ", ".join(r.get_ranked_phrases()[:10])


folder_name = ".\\filtered_texts"
file_names = os.listdir(folder_name)
data = ""
for i in range(len(file_names)):
    data += file_names[i][:file_names[i].index(".")]+"\n"
    data += extract_kw(folder_name+"\\"+file_names[i])+"\n"
    data += "\n"

open("out.txt","w").write(data)
    
    

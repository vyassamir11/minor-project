import re,enchant,string
import numpy as np
from textblob import TextBlob
from collections import Counter
from nltk.corpus import stopwords
dictionary = enchant.Dict("en_US")

#ignore log zero warning
np.seterr(divide='ignore')

def remove_html_and_spec(string):
    return re.sub('\s+',' ',re.sub('\\\\r\\\\n',' ',re.sub('<[^>]*>',' ',string)))

def get_data_and_headers(data):
    
    bet_line_breaks = ' '.join(re.findall('<br>[^>]*<br>',data))
    bet_paras = ' '.join(re.findall('<p>[^>]*</p>',data))
    bet_headers = ' '.join(re.findall('<h[0-9]>[^>]*</h[0-9]>',data))
    bet_ol_headers = ' '.join(re.findall('<ol>[^>]*</ol>',data))
    bet_ul_headers = ' '.join(re.findall('<ul>[^>]*</ul>',data))
    bet_li = ' '.join(re.findall('<li>[^>]*</li>',data))
    bet_span_toctext = ' '.join(re.findall('<span class="toctext">[^>]*</span>',data))
    bet_strong = ' '.join(re.findall('<strong>[^>]*</strong>',data))

    data = ''
    data += remove_html_and_spec(bet_line_breaks)+" "
    data += remove_html_and_spec(bet_paras)+" "
    data += remove_html_and_spec(bet_li)

    headers = []
    headers.extend(remove_html_and_spec(bet_headers).split())
    headers.extend(remove_html_and_spec(bet_ol_headers).split())
    headers.extend(remove_html_and_spec(bet_ul_headers).split())
    headers.extend(remove_html_and_spec(bet_span_toctext).split())
    headers.extend(remove_html_and_spec(bet_strong).split())

    return (data,headers)

def get_nouns(text):
    if isinstance(text,str):
        blob = TextBlob(text)
    else:
        blob = TextBlob(" ".join(text))
    nouns = []
    for (word,tag) in blob.tags:
        if tag[0] == "N":
            nouns.append(word)
    return nouns

def PMI(category,article_link):
    data = open(article_link,'r').read().lower()
    data,headers = get_data_and_headers(data)
    # data_nouns = get_nouns(data)
    # headers_nouns = get_nouns(headers)

    # headers_count = Counter(headers_noun)
    # data_count = Counter(data_nouns)
    data = data + " ".join(headers)
    allow = string.ascii_letters + string.digits + ' '
    data = re.sub('[^%s]' % allow,' ',data)
    data =re.sub('\d',' ',data)
    data = data.split()
    #consider only english words
    data_size = len(data)
    data = list(filter(lambda x : dictionary.check(x),data))
    data_english_text = " ".join(data)
    #stopword removal
    stop = set(stopwords.words('english'))
    clean_data = set(data) - stop
    clean_data = list(clean_data)
    #consider nouns
    clean_data_nouns = get_nouns(clean_data)

    #PMI init
    clean_data_count = Counter(clean_data_nouns)
    # print(clean_data_count)
    nouns_prob = {}
    for ele in clean_data_nouns:
        nouns_prob[ele] = clean_data_count[ele]/data_size

    # Make this generic
    ind_name = category
    ind_count = data_english_text.count(ind_name)
    ind_prob = ind_count/data_size

    # print(len(data))
    co_occur_prob = nouns_prob.copy() #initialize
    window = 15

    ind_name_split = ind_name.split()
    indices = np.where(np.array(data) == ind_name_split[0])

    for ele in clean_data_nouns:
        c = 0
        for index in indices[0]:
            # start = index-window if index>=window else 0
            end = index+window if index+window<len(data) else len(data)
            line = data[index:end]
            if ele in line:
                c += 1

        co_occur_prob[ele] = c/data_size

    pmi = co_occur_prob.copy()



    for ele in clean_data_nouns:
        pmi[ele] = np.log(co_occur_prob[ele]/(nouns_prob[ele]*ind_prob))
        # print(ele + " : " + str(pmi[ele]))
    print()
    print(Counter(pmi).most_common(13))

categories = ['cash crop','food crop','beverages','wood','crop','tobacco','petroleum']
for _category in categories:
    PMI(_category,str(_category)+".html")